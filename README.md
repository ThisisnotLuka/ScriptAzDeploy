# ScriptAzDeploy

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/ThisisnotLuka/ScriptAzDeploy.git
git branch -M main
git push -uf origin main
```

# Projet de Déploiement de Machine Virtuelle Azure avec Bicep

## Description
Ce projet utilise Azure Bicep pour déployer une machine virtuelle (VM) Ubuntu sur Azure. Il inclut la création automatique de toutes les ressources nécessaires telles que le réseau virtuel, l'adresse IP publique, et le groupe de sécurité réseau. L'accent est mis sur l'utilisation de clés SSH pour l'authentification sécurisée.

## Prérequis
- Compte Azure
- [Azure CLI](https://docs.microsoft.com/en-us/cli/azure/install-azure-cli)
- Clé SSH publique

## Installation et Configuration
1. **Installer Azure CLI**: Suivez les instructions sur la [page d'installation de Azure CLI](https://docs.microsoft.com/en-us/cli/azure/install-azure-cli).

2. **Connexion à Azure**:
   ```bash
   az login
   ```

3. **Generer la clé**:
   ```bash
   ssh-keygen -t rsa -b 4096
   ```

# Usage
## Configurer le fichier Bicep:

Modifiez Fichier.bicep avec vos paramètres spécifiques.
Remplacez adminUsername et adminPasswordOrKey avec votre nom d'utilisateur et votre clé SSH publique.

##Déployer la VM:

Exécutez la commande suivante :
```bash 
az deployment group create --resource-group <NomDuGroupeDeRessources> --template-file ./main.bicep --parameters adminUsername=<VotreNomUtilisateur> adminPasswordOrKey="<VotreCléSSHPublique>"
```

## Connexion à la VM

Utilisez la commande SSH fournie en sortie pour vous connecter à votre VM.
```bash
ssh -i <chemin/de/la/key.pem> <username>@<IpVM>
```

# Nettoyage

Pour supprimer toutes les ressources créées :
```bash 
az group delete --name <NomDuGroupeDeRessources> --yes --no-wait
```

#Contribution
Les contributions sont les bienvenues. Suivez les bonnes pratiques de développement et de documentation.




