#!/bin/bash

# Azure CLI VM Management Script

# Function to list available VM images
list_vm_images() {
    echo "Listing available VM images..."
    az vm image list --output table
}

# Function to create a resource group if it doesn't exist
create_resource_group_if_not_exists() {
    local group_name=$1
    local group_location=$2

    if [ "$(az group exists --name "$group_name")" = "false" ]; then
        echo "Resource group $group_name does not exist. Creating resource group..."
        az group create --name "$group_name" --location "$group_location"
    else
        echo "Resource group $group_name already exists."
    fi
}

# Function to create a VM
create_vm() {
    read -p "Enter Resource Group Name: " resource_group
    read -p "Enter VM Name: " vm_name
    read -p "Enter VM Size (e.g., Standard_B1s): " vm_size
    read -p "Enter Image URN (e.g., UbuntuLTS): " image_urn
    read -p "Enter Location (e.g., eastus): " location

    create_resource_group_if_not_exists "$resource_group" "$location"

    echo "Creating VM..."
    az vm create --name "$vm_name" --resource-group "$resource_group" \
                 --size "$vm_size" --image "$image_urn" --location "$location" \
                 --generate-ssh-keys
    echo "VM $vm_name created successfully."
}

# Function to deallocate a VM
deallocate_vm() {
    read -p "Enter VM Name: " vm_name
    read -p "Enter Resource Group Name: " resource_group

    echo "Deallocating VM $vm_name..."
    az vm deallocate --name "$vm_name" --resource-group "$resource_group"
    echo "VM $vm_name deallocated."
}

# Function to delete a VM
delete_vm() {
    read -p "Enter VM Name: " vm_name
    read -p "Enter Resource Group Name: " resource_group

    echo "Deleting VM $vm_name..."
    az vm delete --name "$vm_name" --resource-group "$resource_group" --yes
    echo "VM $vm_name deleted."
}

list_vms_and_resources() {
    echo "Listing all VMs and their associated resources..."
    az vm list --show-details --output table
}

delete_resource_group() {
    read -p "Enter Resource Group Name to delete: " resource_group

    echo "Deleting resource group $resource_group and all its resources..."
    az group delete --name "$resource_group" --yes --no-wait
    echo "Resource group $resource_group deleted."
}

# Main menu
while true; do
    echo "Azure VM Management Script"
    echo "1. List VM images"
    echo "2. Create VM"
    echo "3. Deallocate VM"
    echo "4. Delete VM"
    echo "5. List all VMs and resources"
    echo "6. Delete Resource Group"
    echo "7. Exit"
    read -p "Select an option: " option

    case $option in
        1) list_vm_images ;;
        2) create_vm ;;
        3) deallocate_vm ;;
        4) delete_vm ;;
        5) list_vms_and_resources ;;
        6) delete_resource_group ;;
        7) break ;;
        *) echo "Invalid option. Please try again." ;;
    esac
done